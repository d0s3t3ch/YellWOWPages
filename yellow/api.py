from quart import render_template, request, redirect, url_for, jsonify, Blueprint, abort, flash, send_from_directory, current_app

import settings
from yellow.models import User

bp_api = Blueprint('bp_api', __name__, url_prefix='/api')


@bp_api.get("/")
async def api_root():
    return await render_template('api.html')


@bp_api.get('/user/')
async def api_all():
    q = User.select()
    q = q.where(User.address.is_null(False))
    return jsonify([u.to_json(ignore_key='id') for u in q])


@bp_api.get('/user/<path:needle>')
async def api_search(needle: str):
    try:
        return jsonify([u.to_json(ignore_key='id') for u in await User.search(needle)])
    except Exception as ex:
        current_app.logger.error(ex)
        return jsonify([])
