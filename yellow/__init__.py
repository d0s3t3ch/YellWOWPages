from quart import session, abort

from functools import wraps


def login_required(func):
    @wraps(func)
    async def wrapper(*args, **kwargs):
        user = session.get('user')
        if not isinstance(user, dict):
            abort(403)
        return await func(*args, **kwargs)
    return wrapper
