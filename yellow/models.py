import os, re, random
from typing import Optional, List
from datetime import datetime

from peewee import SqliteDatabase, SQL, ForeignKeyField
import peewee as pw

import settings

db = SqliteDatabase(settings.DB_PATH)


class User(pw.Model):
    id = pw.UUIDField(primary_key=True)
    created: datetime = pw.DateTimeField(default=datetime.now)
    username = pw.CharField(unique=True, null=False)
    address = pw.CharField(null=True)

    @property
    def created_dt(self):
        return self.created.strftime('%Y-%m-%d')

    @staticmethod
    async def search(needle) -> List['User']:
        if not needle:
            raise Exception("need search term")
        needle = needle.replace("*", "")
        needle = needle.lower()

        return User.select().where(
            User.address.is_null(False),
            User.username % f"*{needle}*"
        )

    def to_json(self, ignore_key=None):
        data = {
            "id": self.id,
            "username": self.username,
            "address": self.address
        }
        if isinstance(ignore_key, str):
            data.pop(ignore_key)
        return data

    class Meta:
        database = db
