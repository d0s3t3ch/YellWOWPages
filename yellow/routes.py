from quart import render_template, request, redirect, url_for, jsonify, Blueprint, abort, flash, send_from_directory, session
import re

from yellow import login_required
from yellow.models import User

bp_routes = Blueprint('bp_routes', __name__)


@bp_routes.get("/")
async def root():
    return await render_template('index.html')


@bp_routes.route("/login")
async def login():
    from yellow.factory import keycloak
    return redirect(url_for(keycloak.endpoint_name_login))


@bp_routes.route("/logout")
@login_required
async def logout():
    session['user'] = None
    return redirect(url_for('bp_routes.root'))


@bp_routes.route("/dashboard")
@login_required
async def dashboard():
    return await render_template('dashboard.html')


@bp_routes.post("/dashboard/address")
@login_required
async def dashboard_address_post():
    form = await request.form
    address = form.get('address')
    if len(address) not in [97, 108]:
        raise Exception("Please submit a WOW address")

    # update user
    from yellow.models import User
    user = User.select().filter(User.id == session['user']['id']).get()
    user.address = address
    user.save()
    session['user'] = user.to_json()

    return await render_template('dashboard.html')


@bp_routes.post("/dashboard/address/delete")
@login_required
async def dashboard_address_delete():
    from yellow.models import User
    user = User.select().filter(User.id == session['user']['id']).get()
    user.address = None
    user.save()
    session['user'] = user.to_json()
    return redirect(url_for("bp_routes.dashboard"))


@bp_routes.route("/search")
async def search():
    needle = request.args.get('username')
    if needle:
        users = [u for u in await User.search(needle)]
        if users:
            return await render_template('search_results.html', users=users)
        else:
            return await render_template('search_results.html')

    q = User.select()
    q = q.where(User.address.is_null(False))
    q = q.limit(100)

    users = [u for u in q]
    return await render_template('search.html', users=users)


@bp_routes.route("/user/<path:name>")
async def user_page(name: str):
    if not name:
        raise Exception("invalid name")
    name = name.lower()

    try:
        _user = User.select().where(
            User.username == name,
            User.address.is_null(False)
        ).get()
    except:
        return abort(404)

    return await render_template('user.html', users=[_user])


@bp_routes.route("/about")
async def about():
    return await render_template('about.html')
