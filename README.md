## YellWOWPages

Wownero yellow pages web-application - lookup the WOW address of users.

### Installation

```bash
git clone gitea@git.wownero.com:wownero/YellWOWPages.git
cd YellWOWPages
pip install -r requirements.txt

cp settings.py_example settings.py
```

Change `settings.py` to your liking. Then run the application:

```bash
python3 run.py
```
